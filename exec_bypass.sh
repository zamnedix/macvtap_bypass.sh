#!/usr/bin/env bash

#    exec_bypass.sh - Run command inside a network namespace
#    Copyright (C) 2018  zamnedix

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

syntax() {
	IFS='' read -r -d '' output << EOF
Syntax: $0 [--config file] [--netns namespace] 
  [--username username] [--group groupname] [--preserve vars] <command>

The following value MUST be defined, in a config file or on the command line:
  --netns             | Name of network namespace
Optional arguments:
  --config <path>     | Path to config file - the following command-line
                      | arguments will override any values found in the 
                      | config file
  --username <name>   | Username to run command under (default current user)
  --group    <name>   | Group to run command under (default current group)
  --preserve <a,b...> | Comma-separated list of environment variables to preserve
                      | (default none)
Summary: This script executes arbitrary commands inside the confines of a 
  network namespace created by the companion script macvlan_bypass.sh
EOF
	echo -en "$output"
	exit 255
}

arg="$1"
while [ "${arg:0:1}" == "-" ]; do
	arg="${arg:1:$((${#arg}-1))}"
	if [ "$arg" == "h" ] || [ "$arg" == "-help" ]; then
		syntax
	elif [ "$arg" == "-config" ]; then
		shift
		config="$1"
	elif [ "$arg" == "-netns" ]; then
		shift
		netns="$1"
	elif [ "$arg" == "-username" ]; then
		shift
		username="$1"
	elif [ "$arg" == "-group" ]; then
		shift
		group="$1"
	elif [ "$arg" == "-preserve" ]; then
		shift
		preserve="$1"
	fi
	shift
	arg="$1"
done

if [ -z "$1" ]; then
	syntax
else
	command="$@"
fi

if [ ! -z "$config" ]; then
	while read line; do
		if [ -z "$line" ]; then
			break
		fi
		line="${line// /}"
		key="${line%%=*}"
		if [ ! -z "${!key}" ]; then
			echo "[warn] $0: main: skipping config key \`$key' - already defined"
		else
			value="${line##*=}"
			export ${key}="$value"
		fi
	done < "$config"
fi

if [ -z "$netns" ]; then
	echo "[fatal] $0: Network namespace not defined"
	syntax
fi
if [ -z "$username" ]; then
	export username="$(id -n -u)"
fi
if [ -z "$group" ]; then
	export group="$(id -n -g)"
fi

eval "sudo --preserve-env='${preserve}' ip netns exec ${netns} sudo --preserve-env='${preserve}' -u ${username} -g ${group} ${command}"
