This script is used to create an isolated network namespace
with a new macvlan interface bridged to an existing primary interface.
The secondary interface is configured with an alternate set of
addresses and routes.

# What?

Essentially, this tools purpose is to "split a Linux ethernet device in two". 
First, a "macvlan" interface is created *on top* of an existing ethernet interface.
The secondary interface has a different MAC address, but sends and receives packets
through the same physical link. 

Now, a new "network namespace" is created. This is essentially a new, separate copy of the
entire networking stack - interfaces, routing, and all. The macvlan interface we just made is
moved inside this namespace.

Finally, regular applications can be launched inside the new network namespace with "exec_bypass.sh". 
They will use the routing tables, firewall rules, and IP addresses of the namespace.

# Why?

I have a VPN gateway as my default route. This gateway is blocked by some video streaming services.
I initially designed this script so I could route traffic differently for a single application.
It also works well (in --bridge-mode) as a way to isolate virtualized hosts (and groups of hosts.)

# Why not policy routing?

It works fine, but I tried it and I don't like how it feels. My solution is more complex to configure, but I 
believe it yields a much more practical and useful result than anything policy routing can do for you. Inside 
a new network namespace, you have your own routing tables, iptables, ebtables, network interfaces and sysctls. 

# Examples

### Absolute simplest example
```shell
user@host $ sudo ./macvlan_bypass.sh --primary eth0 --netns bypass
user@host $ ./exec_bypass.sh --netns bypass firefox
user@host $ sudo ./macvlan_destroy.sh bypass
```
* The above example creates a new network namespace and a new macvlan device enslaved to the eth0 interface. It grabs an IPv4 address from DHCP and auto-generates an IPv6 address with slaac. Google Public DNS is used for the resolver.
* Firefox is launched inside the namespace with exec_bypass.sh.
* When we're done, we destroy the namespace and associated resources with macvlan_destroy.sh.

### Sound doesn't work!
```shell
user@host $ sudo ./macvlan_bypass.sh --primary eth0 --netns bypass
user@host $ PULSE_SERVER=/run/user/$(id -u)/pulse/native ./exec_bypass.sh \
            --preserve PULSE_SERVER --netns bypass firefox
user@host $ sudo ./macvlan_destroy.sh bypass
```
* We follow the same process as above, but specify and preserve the PULSE_SERVER environment variable so Firefox can find the Pulseaudio server.

Both "macvlan_bypass.sh" and "exec_bypass.sh" support a simple config file syntax. See the .cfg files
in this repo for some examples. Any command-line arguments passed to either script will override any
values found in a a config file.