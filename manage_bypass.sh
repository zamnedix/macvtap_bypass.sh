#!/usr/bin/env bash

#    manage_bypass.sh - Perform various administration tasks for network namespaces
#    Copyright (C) 2018  zamnedix

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

syntax() {
    IFS='' read -r -d '' output << EOF
Syntax: $0 <namespace> <action> [args...]

  namespace | Network namespace to act on
  
Actions:
  * isolate
    - Drop all incoming and outgoing packets to/from the namespace
      (only affects interfaces in namespace that existed before running this 
      command)
  * integrate
    - Allow traffic to pass through the namespace once more
      (same limitations apply)
!!! Warning !!!
  Do not use the features above as security restrictions! It is trivial
  to change the MAC address of network interfaces from within the namespace
!!! !!!!!!! !!!
  * attach <namespace> 
    - Link two namespaces with a virtual ethernet pair
  * detach <namespace>
    - Destroy the virtual ethernet pair linking the two namespaces
  * link <namespace> <primary address> <secondary address>
    - Configure addresses and routes for two sides of a virtual ethernet
      pair
  * unlink <namespace> <primary address> <secondary address>
    - Deconfigure both sides of a virtual ethernet pair    
EOF
    echo -en "$output"
    exit 255
}

get_all_macs() {
	all_ifs="$(ip netns exec ${namespace} ls /sys/class/net)"
	for if in $all_ifs; do
		type="$(ip netns exec ${namespace} cat /sys/class/net/${if}/type)"
		#Exclude loopback devices
		if [ "$type" != "772" ]; then 
			t="$(ip netns exec ${namespace} cat /sys/class/net/${if}/address)"
			all_macs="$t $all_macs"
		fi
	done
	all_macs="${all_macs% *}"
	echo "${all_macs// /,}"
}

if [ -z "$2" ]; then
    syntax
fi

if [ "$(id -u)" -ne "0" ]; then
	echo "[warn] $0: main: this script may require root privileges"
fi

namespace="$1"
ip -n "$namespace" l &> /dev/null
if [ "$?" -ne "0" ]; then
	echo "[fatal] $0: main: no such network namespace \`$namespace'"
	exit 1
fi
action="$2"

trap "exit" ERR

case "$action" in
	isolate)
		all_macs="$(get_all_macs)"
		ebtables --concurrent -A INPUT --among-dst "$all_macs" -j DROP
		ebtables --concurrent -A OUTPUT --among-src "$all_macs" -j DROP
		echo "[info] $0: main: isolated interfaces [${all_macs//,/, }] at layer 2"
		;;
	integrate)
		all_macs="$(get_all_macs)"
		ebtables --concurrent -D INPUT --among-dst "$all_macs" -j DROP
		ebtables --concurrent -D OUTPUT --among-src "$all_macs" -j DROP
		echo "[info] $0: main: reintegrated interfaces [${all_macs// /, }] at layer 2"
		;;
	attach)
		if [ -z "$3" ]; then
			syntax
		fi
		target="$3"
		ifname="${namespace}-${target}"
		ip l add "$ifname" type veth peer name "$ifname" netns "$namespace"
		ip l set "$ifname" netns "$target"
		ip -n "$namespace" l set "$ifname" up
		ip -n "$target" l set "$ifname" up
		;;
	detach)
		if [ -z "$3" ]; then
			syntax
		fi
		target="$3"
		ifname="${namespace}-${target}"
		ip -n "$namespace" l del "$ifname"
		;;
 	link)
		if [ -z "$3" ] || [ -z "$4" ] || [ -z "5" ]; then
			syntax
		fi
		target="$3"
		ifname="${namespace}-${target}"
		primary="$4"
		secondary="$5"
		ip -n "$namespace" a add "$primary" dev "$ifname"
		ip -n "$namespace" r add "$secondary" dev "$ifname"
		ip -n "$target" a add "$secondary" dev "$ifname"
		ip -n "$target" r add "$primary" dev "$ifname"
		echo -e "$secondary\t$target" >> /etc/netns/"$namespace"/hosts
		echo -e "$primary\t$namespace" >> /etc/netns/"$target"/hosts
		;;
	unlink)
		if [ -z "$3" ] || [ -z "$4" ] || [ -z "5" ]; then
			syntax
		fi
		target="$3"
		ifname="${namespace}-${target}"
		primary="$4"
		secondary="$5"
		ip -n "$namespace" a del "$primary" dev "$ifname"
		#ip -n "$namespace" r del "$secondary" dev "$ifname"
		ip -n "$target" a del "$secondary" dev "$ifname"
		#ip -n "$target" r del "$primary" dev "$ifname"
		sed -i 's|^'"${secondary}"'.*$||' /etc/netns/"$namespace"/hosts
		sed -i 's|^'"${primary}"'.*$||' /etc/netns/"$target"/hosts
		;;
	*)
		syntax
		;;
esac

echo "[info] $0: main: completed successfully"
