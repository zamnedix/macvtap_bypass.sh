#!/usr/bin/env bash

#Script assumes a second firefox profile exists named "2"
#Any arguments passed to this script are passed as extra arguments to Firefox

trap 'exit' ERR
sudo ./macvlan_bypass.sh --config bypass.cfg
./exec_bypass.sh --config execbypass.cfg firefox -no-remote -P 2 $@
sudo ./macvlan_destroy.sh bypass
