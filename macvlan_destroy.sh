#!/usr/bin/env bash

#    macvlan_destroy.sh - Destroy network namespaces created by macvlan_bypass.sh
#    Copyright (C) 2018  zamnedix

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

syntax() {
    IFS='' read -r -d '' output << EOF
Syntax: $0 <netns>

		netns | Name of network namespace

Summary: This script is used to destroy a network namespace created by 
		 macvlan_bypass.sh and all it's associated resources. All PIDS
		 running inside the namespace are killed as well.

EOF
    echo -en "$output"
    exit 255
}

if [ -z "$1" ]; then
    syntax
fi

export netns="$1"

status=0
while read pid; do
	if [ -z "$pid" ]; then
		break;
	fi
	kill -9 "$pid"
	((status+="$?"));
done <<< "$(ip netns pids $netns)"
ip netns del "$netns"
((status+="$?"));
rm -rf "/etc/netns/$netns"
((status+="$?"));
if [ "$status" -ne "0" ]; then
	echo "[error] $0: main: errors while destroying namespace"
	exit 1
else
	echo "[info] $0: main: namespace $netns destroyed"
	exit 0
fi

